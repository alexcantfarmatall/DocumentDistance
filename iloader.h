#ifndef ILOADER_H
#define ILOADER_H



#include <QString>

class ILoader {

public:
    virtual QString load() = 0;
    virtual ~ILoader(){}
};

#endif

