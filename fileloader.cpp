
#include "fileloader.h"
#include <QString>
#include <QFileDialog>
#include <QTranslator>
#include <QMessageBox>
#include <QDataStream>

#include <iostream>
#include <sstream>

QString FileLoader::load()
{
    QString filename = QFileDialog::getOpenFileName(nullptr,
                                                    QObject::tr("Open Address Book"),
                                                    "/home/Documents",
                                                    QObject::tr("*"));
    QFile file(filename);

    if(filename.isEmpty()) {
       return nullptr;

    }

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::information(nullptr, QObject::tr("unable to open file"),
                                file.errorString());
        return nullptr;
    }

    //change to stringbuilder...

    std::stringstream ss;
    while(!file.atEnd()) {
        ss << file.readLine().toStdString();
    }

    file.close();
    return std::move(QString::fromStdString(ss.str()));

}

FileLoader::FileLoader(){}
FileLoader::~FileLoader() {}


