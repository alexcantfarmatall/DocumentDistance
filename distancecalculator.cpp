#include "distancecalculator.h"
#include <vector>
#include <exception>
#include <math.h>
#include <iostream>
#include <stdexcept>

DistanceCalculator::DistanceCalculator(std::vector<int>& v1, std::vector<int>& v2)
{
    if(v1.size() != v2.size())
    {
        std::cout << "not same len" << std::endl;
        //throw std::invalid_argument("vectors must be the same length");
    }

    distance_rad = documentDistance(v1,v2);

    distance_deg = distance_rad * 180/3.14;
    similarity_percentage = 100 - distance_rad / max_distance * 100;
}

double DistanceCalculator::dotProduct(std::vector<int>& v1, std::vector<int>& v2)
{
    double result = 0;

    for(int i = 0; i < v1.size(); i++)
    {
        result += v1[i] * v2[i];
    }

    return result;
}

double DistanceCalculator::norm(std::vector<int>& v)
{
    double tmp = 0;

    for(int i = 0; i < v.size(); i++)
    {
        tmp += pow(v[i],2);
    }

    double result = std::sqrt(tmp);
    return result;
}

double DistanceCalculator::documentDistance(std::vector<int>& v1, std::vector<int>&v2)
{
    double dotp = dotProduct(v1,v2);
    double len = norm(v1)*norm(v2);
    double ratio = dotp/len;
    double result = ratio <= 1 ? std::acos(dotp/len) : 0;
    return result;
}
double DistanceCalculator::get_distance_rad()const
{
    return distance_rad;
}

double DistanceCalculator::get_distance_deg()
{
    return distance_deg;
}

double DistanceCalculator::get_similarity_percentage()
{
    return similarity_percentage;
}

