#include "documentdistance.h"
#include "ui_documentdistance.h"
#include "graphs.h"
#include <QString>
#include "documentpreparator.h"
#include <iostream>
#include <sstream>
#include <QDialog>
#include <QDataStream>
#include "fileloader.h"

DocumentDistance::DocumentDistance(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DocumentDistance)
{
    ui->setupUi(this);
    graphs = new Graphs(this);
    ui->pushButton_load_text1->setToolTip(tr("load text from file"));
    ui->pushButton_load_text2->setToolTip(tr("load text from file"));
    loader = new FileLoader();
}


DocumentDistance::~DocumentDistance()
{
    delete dc;
    delete dp;
    delete graphs;
    delete ui;
    delete loader;
}



void DocumentDistance::on_pushButton_clicked()
{
    ui->listWidget_input_1->clear();
    ui->listWidget_input_2->clear();
    ui->lineEdit_words_processed->clear();
    if(ui->graphicsView_angleGraph->scene())
        ui->graphicsView_angleGraph->scene()->clear();

    std::string text1 = (ui->textEdit_1->toPlainText()).toStdString();
    std::string text2 = (ui->textEdit_2->toPlainText()).toStdString();

    std::vector<std::string> raw{text1,text2};
    this->dp = new DocumentPreparator(raw);

    std::stringstream ss{};
    int total_word_count = 0;
    for(auto mapentry : dp->word_count_maps[0])
    {
        ss << mapentry.first << " :\t\t" << mapentry.second <<std::endl;
        total_word_count += mapentry.second;
    }


    ui->listWidget_input_1->addItem(QString::fromStdString(ss.str()));

    ss.str(std::string());

    for(auto mapentry : dp->word_count_maps[1])
    {
        ss << mapentry.first << " :\t\t" << mapentry.second <<std::endl;
        total_word_count += mapentry.second;
    }

    ui->lineEdit_words_processed->setText(QString::number(total_word_count));
    ui->listWidget_input_2->addItem(QString::fromStdString(ss.str()));

    //get the angle

    this->dc = new DistanceCalculator(dp->sorted_frequencies[0], dp->sorted_frequencies[1]);


    ui->lineEdit_angle->setText(QString::number(dc->get_distance_deg()));
    ui->lineEdit_percentage->setText(QString::number(dc->get_similarity_percentage()));

    double width = ui->graphicsView_angleGraph->geometry().width();
    double height = ui->graphicsView_angleGraph->geometry().height();

    graphs->angle_graph(dc->get_distance_rad(), width, height);

    ui->graphicsView_angleGraph->setScene(graphs->angle_scene);
    ui->graphicsView_angleGraph->show();
}

void DocumentDistance::on_pushButton_Chart_clicked()
{
    //chart = new WordCountGraph(this);
    //pass a pointer to dp

    graphs->make_barchart(this->dp->sorted_frequencies, this->dp->corpus_set);
}



void DocumentDistance::on_pushButton_load_text1_clicked()
{
    QString res = loader->load();

    ui->textEdit_1->clear();
    ui->textEdit_1->setText(res);
    loader->~ILoader(); //order: ~Fileloader, ~ILoader
}

void DocumentDistance::on_pushButton_load_text2_clicked()
{
    QString res = loader->load();

    ui->textEdit_2->clear();
    ui->textEdit_2->setText(res);
}
