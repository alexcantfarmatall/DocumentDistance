/*
 * DocumentPreparator takes two argument strings in a constructor
 *  strings are cleaned of all but alphanumeric chars
 *
 * for each string passed in, you get out:
 *      word_count_maps...........vector<map<string,int>>.....(word and it's frequency)
 *          index 0 for the first arg, index n for the nth arg
 *
 *      corpus_set................vector<int>.....(sorted set of all the words passed in)
 *
 *      frequencies...............vector<vector<int>> words frequencies for each document passed in,
 *          in the context of corpus.
 *          Since all the vectors are in the context of corpus they are all of the same size
 *
 *          If a word at corpus[i] contains word "fish", then if nth input string contains word fish 3 times,
 *          frequencies[n][i] = 3, or frequencies[n][i] = 0, if nth input didnt contain word "fish" at all
 *
 *
*/
#ifndef DOCUMENTPREPARATOR_H
#define DOCUMENTPREPARATOR_H

#include <string>
#include <unordered_map>
#include <vector>
#include <set>
class DocumentPreparator{

    //corpus_set_maker function makes a sorted set of ALL the program's input words
    //return value will eventually be used in making of frequency vectors for each document
    std::set<std::string> corpus_set_maker(std::vector<std::vector<std::string>>& material);

    //make_frequency_maps function includes all the words from param vectors to each's corresponding map
    //where key is the word and value is the number of times it occured in a given text
    std::vector<std::unordered_map<std::string,int>> make_frequency_maps(std::vector<std::vector<std::string>>& material);

    //make_frequency_vectors function...
    std::vector<std::vector<int>> make_frequency_vectors();

    //clean function rids the docs in param vectors' words of all but alphanumeric chars
    std::vector<std::vector<std::string>> clean(std::vector<std::string>& text);
    void trim(std::string&);
public:

    std::vector<std::vector<int>> sorted_frequencies;

    std::set<std::string> corpus_set;

    std::vector<std::unordered_map<std::string,int>> word_count_maps;

    explicit DocumentPreparator(std::vector<std::string>&);

    //definition to source file??
    DocumentPreparator& operator=(const DocumentPreparator& dp){
        sorted_frequencies = dp.sorted_frequencies;
        corpus_set = dp.corpus_set;
        word_count_maps = dp.word_count_maps;
        return *this;
    }

    virtual ~DocumentPreparator(){}
};


#endif // DOCUMENTPREPARATOR_H
