#include"documentpreparator.h"
#include<iterator>
#include<istream>
#include<iostream>
#include<sstream>
#include<QWidget>
#include<unordered_map>
#include<vector>
#include<set>
#include<numeric>
#include<iterator>

DocumentPreparator::DocumentPreparator(std::vector<std::string>& raw){

    if(raw.size() == 2)
    {
        std::vector<std::vector<std::string>> cleaned = clean(raw);
        corpus_set = corpus_set_maker(cleaned);
        word_count_maps = make_frequency_maps(cleaned);
        // Make sure output vectors are the same length here, and throw if not
        sorted_frequencies = make_frequency_vectors();
    }
    else if(raw.size() > 2)
    {
        std::cerr << "tf-idf search not implemented yet..." <<std::endl;
    }
    else
    {
        std::cerr << "invalid arguments, std::vector<string>&(size()==2) expectet" << std::endl;
    }

}


std::vector<std::vector<std::string>> DocumentPreparator::clean(std::vector<std::string>& texts)
{
    std::stringstream ss{};
    std::vector<std::vector<std::string>> result{};

    for(std::string text : texts)
    {
        std::vector<std::string> tmp{};


        for(char c : text)
        {
            if(std::isalpha(c))
            {
                c = std::tolower(c);
                ss << c;

            }
            else if(std::isdigit(c))
            {
                ss << c;

            }
            else if(ss.str().length() > 0)
            {
                tmp.push_back(ss.str());
                ss.str(std::string());
            }

        }
        if(ss.str().length() > 0)
        {
            tmp.push_back(ss.str());
        }
        ss.str(std::string());

        result.push_back(tmp);
    }

    return result;
}

std::vector<std::unordered_map<std::string,int>>
DocumentPreparator::make_frequency_maps(std::vector<std::vector<std::string>>& material)
{
    std::vector<std::unordered_map<std::string, int>> result;
    for(std::vector<std::string> words : material)
    {
        std::unordered_map<std::string,int> tmp;
        for(std::string s : words)
        {

                if(tmp.insert(std::make_pair(s,1)).second)
                {
                    std::cout << "added: " << s << std::endl;
                }
                else
                {
                    int count = tmp.at(s)+1;
                    tmp[s] = count;
                    std::cout << "changed: " << s << " -> " << count << std::endl;
                }
        }
        result.push_back(tmp);
    }

    return result;
}


std::set<std::string> DocumentPreparator::corpus_set_maker(std::vector<std::vector<std::string>>& material)
{
    std::set<std::string> result;

    for(std::vector<std::string> v : material)
    {
        for(std::string s : v)
        {
            result.insert(s);
            //std::cout<< result[s] << std::endl;
        }
    }


    return result;
}

//corpus_set and frequency_maps have to be ready before this can work
std::vector<std::vector<int>> DocumentPreparator::make_frequency_vectors(){

    std::vector<std::vector<int>> result;

    for(std::unordered_map<std::string,int> local_map : this->word_count_maps)
    {
        std::vector<int> tmp;
        for(auto s : corpus_set)
        {
             std::unordered_map<std::string,int>::const_iterator got = local_map.find(s);

             if(got == local_map.end())
             {
                 tmp.push_back(0);
             }
             else
             {
                 tmp.push_back(got->second);
             }
        }

        result.push_back(tmp);
    }
    return result;
}

void DocumentPreparator::trim(std::string& inputString)
{
    std::istringstream stringStream(inputString);
    std::vector<std::string> tokens((std::istream_iterator<std::string>(stringStream)),
                                    std::istream_iterator<std::string>());

    inputString = std::accumulate(std::next(tokens.begin()), tokens.end(),
                                    tokens[0], // start with first element
                                    [](std::string a, std::string b) { return a + " " + b; });
}













