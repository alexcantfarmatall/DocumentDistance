#ifndef DOCUMENTDISTANCE_H
#define DOCUMENTDISTANCE_H

#include <QMainWindow>
#include <QString>
#include <documentpreparator.h>
#include <distancecalculator.h>
#include "graphs.h"
#include "iloader.h"

namespace Ui {
class DocumentDistance;
}

class DocumentDistance : public QMainWindow
{
    Q_OBJECT

public:
    explicit DocumentDistance(QWidget *parent = 0);
    ~DocumentDistance();
    void on_actionClear_triggered();
    void on_actionCompare_triggered();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_Chart_clicked();

    void on_pushButton_load_text1_clicked();

    void on_pushButton_load_text2_clicked();

private:
    DocumentPreparator* dp;
    Ui::DocumentDistance *ui;
    DistanceCalculator* dc;
    Graphs* graphs;
    ILoader* loader;
};

#endif // DOCUMENTDISTANCE_H
