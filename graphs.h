#ifndef GRAPHS_H
#define GRAPHS_H

#include <set>
#include <vector>
#include <string>
#include <QtCharts/QChartView>
#include <QtCharts/QChart>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QKeyEvent>

class Graphs : public QtCharts::QChartView
{

public:
    explicit Graphs(QWidget *parent = 0);
    ~Graphs();
    void make_barchart(std::vector<std::vector<int>>&, std::set<std::string>&);
    void angle_graph(double, double, double);

    QGraphicsScene* angle_scene;

    void keyPressEvent(QKeyEvent*);

private:

    QtCharts::QChart* local_chart;

protected:
    void keyPressEvent(QEvent* event);

};

#endif // GRAPHS_H
