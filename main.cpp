#include "documentdistance.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication DocDistApp(argc, argv);
    DocumentDistance DocDist;
    DocDist.show();

    //QApplication enters it's event loop
    return DocDistApp.exec();
}
