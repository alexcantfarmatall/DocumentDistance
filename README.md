School project

====================================================================

about

Program tells how similar two text documents are, in terms of distance
ranging from 0 - PI/2, where zero means they're similar, 
and 1,57 means they have no words in common.

===================================================================

status

File browser added can read text based files into program...

Bar chart arrow key navigation added, zoom in/out with +/- keys,
default view with space. 



Next: angle graphics should be prettier. ChartView should zoom in
towards y-axis zero-level. Negative y-axis could be eliminated, since
bars cant have negative values.
File input...

=======================================================================

info

Program takes in two texts, cleans them up, leaving only alphanumeric
characters.

Both clean texts are then mapped to word:count maps, words are all
the words from the text, and count means how many times it occured
in the text.

set<string> corpus contains all the words that occured in both inputs,
  in alphabetic order.

vector<int> frequency is created for both input texts.

example:

input strings a and b

A = "this is input string A, string A is an input."
B = "this is input text b, text b is a string"

map<string, int> AfrequencyMap = 
    {a:2, an:1, input:2, is:2, string:2, this:1}

map<string, int> BfrequencyMap =
    {a:1, b:2, input:1, is:2, string:1, text:2, this:1}

corpus = {a, an, b, input, is, string, text, this}

vector<int> A = [2,1,0,2,2,2,0,1]
vector<int> B = [1,0,2,1,2,1,2,1]

Document distance is calculated with A and B

angle of the vectors A and B

distance = acos(dotProduct(A, B)/(length(A)*length(B)))
  
if the input texts are identical distance/angle is zero.
if the input text have no common words, angle is pi/2.

===========================================================


-aleksi anttila
