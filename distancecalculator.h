#ifndef DISTANCECALCULATOR_H
#define DISTANCECALCULATOR_H

#include<vector>
#include <math.h>
class DistanceCalculator
{
    double dotProduct(std::vector<int>&, std::vector<int>&);
    double norm(std::vector<int>&);
    double documentDistance(std::vector<int>&, std::vector<int>&);
    const double max_distance = M_PI/2;
    double distance_rad;
    double distance_deg;
    double similarity_percentage;
public:
    double get_distance_rad()const;
    double get_similarity_percentage();
    double get_distance_deg();
    DistanceCalculator(std::vector<int>&, std::vector<int>&);



};

#endif // DISTANCECALCULATOR_H
