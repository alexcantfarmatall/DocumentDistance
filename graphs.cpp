#include "graphs.h"
#include "ui_wordcountgraph.h"
#include <QtCharts/QBarSet>
#include <QtCharts/QBarSeries>
#include <QtCharts/QChart>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include <QtCharts/QChartView>
#include <QValueAxis>

#include <QMainWindow>
#include <QStringList>
#include <QPainter>

#include <QList>
#include <set>
#include <QString>
#include <math.h>
#include <iostream>

#include <QGraphicsLineItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QKeyEvent>

#include <QRect>
#include <QTransform>

Graphs::Graphs(QWidget *parent)
{
    this->angle_scene = new QGraphicsScene(parent);
}

Graphs::~Graphs()
{
  delete chart();
  delete angle_scene;
  delete local_chart;
}

void Graphs::make_barchart(std::vector<std::vector<int>>& freqs, std::set<std::string>& corpus){

    // Move check to documentPreparator
    if(freqs.size() != 2)
    {
        return;
    }
    QList<QtCharts::QBarSet*> qbars;
    //delete series somewhere...?
    QtCharts::QBarSeries* series = new QtCharts::QBarSeries();


    qbars.append(new QtCharts::QBarSet("input1"));
    qbars.append(new QtCharts::QBarSet("input2"));
    int max = 0;

    for(int i = 0; i < 2; i++)
    {
        std::for_each(freqs[i].begin(), freqs[i].end(), [&qbars, i, &max](int count){
            //std::cout << "adding freq: " << count << std::endl;
            qbars[i]->append(count);
            // Find the highest bar from two vectors for scaling y axis
            max = max < count ? count : max;
        });

        series->append(qbars[i]);
    }

    this->setChart(new QtCharts::QChart());



    chart()->addSeries(series);
    chart()->setTitle("word/count");
    chart()->setAnimationOptions(QtCharts::QChart::SeriesAnimations); //what this do?

    //word tags to the xaxis
    QStringList categories;

    std::for_each(corpus.begin(), corpus.end(), [&categories](std::string word){
        //std::cout << "adding word: " << word << std::endl;
        categories.append(QString::fromStdString(word));
    });


    QtCharts::QBarCategoryAxis* xaxis = new QtCharts::QBarCategoryAxis();

    xaxis->append(categories);
    chart()->createDefaultAxes();
    chart()->setAxisX(xaxis,series);

    chart()->axisX()->setLabelsAngle(60);

    chart()->legend()->setVisible(true);
    chart()->legend()->setAlignment(Qt::AlignTop);

    this->setRubberBand(QtCharts::QChartView::HorizontalRubberBand);
    this->setRenderHint((QPainter::Antialiasing));

    this->setFocus();
    this->resize(1500,300);
    this->show();
    this->setVisible(true);
}

void Graphs::angle_graph(double angle_rad, double width, double height)
{
    double cross = std::sqrt(std::pow(width,2) + std::pow(height,2));

    double anglex = std::cos(angle_rad) * cross;
    double angley = std::sin(angle_rad) * -cross;
    QGraphicsLineItem* vertical_line = new QGraphicsLineItem(0,0,0,-height);
    QGraphicsLineItem* horizontal_line = new QGraphicsLineItem(0,0,width,0);
    QGraphicsLineItem* angle_line = new QGraphicsLineItem(0,0,anglex,angley);
    this->angle_scene->addItem(vertical_line);
    this->angle_scene->addItem(horizontal_line);
    this->angle_scene->addItem(angle_line);
}

void Graphs::keyPressEvent(QKeyEvent* event)
{
    static int xOffset;
    static int yOffset;
    const int moveStep = 30;
    switch(event->key())
    {
    case Qt::Key_Plus:
        chart()->zoomIn();
        break;

    case Qt::Key_Minus:
        chart()->zoomOut();
        break;

    case Qt::Key_Right:
        xOffset += moveStep;
        this->chart()->scroll(moveStep,0);
        std::cout << "xoffset/right: " << xOffset<< std::endl;
        break;

    case Qt::Key_Left:
        // Bind to viewport/elsewehere/qgraphicsviewstate instead of these vars
        if(xOffset > 0) {
            xOffset -= moveStep;
            chart()->scroll(-moveStep,0);
        }
        else
            xOffset = 0;
        std::cout << "xoffset/left: " << xOffset<< std::endl;
        break;

    case Qt::Key_Up:
        yOffset += moveStep;
        chart()->scroll(0,30);
        std::cout << "yoffset/up: " << yOffset<< std::endl;
        break;

    case Qt::Key_Down:
        //local_chart->boundingRect()
        //std::cout << chart()->axisX()->setMin( << std::endl;

        if(yOffset > 0) {
            yOffset -= moveStep;
            chart()->scroll(0,-moveStep);
        }
        else
            yOffset = 0;
        std::cout << "yoffset/down: " << yOffset<< std::endl;
        break;
    case Qt::Key_Space:
        chart()->zoomReset();
        chart()->scroll(xOffset,yOffset);
        break;
    }
}




